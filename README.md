# WebJump Assessment FrontEnd Test

## 1- Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# make sure to run the backend api server from the original project https://itbucket.org/webjump/assessment-frontend/src/master/

# to install dependencies
$ npm install

# run application
$ npm start

# import cors to app.js and write this line

app.use(cors());
```

---

## 2 - Informations about project

### 2.1 -Documentatios libaries

- Nuxt Libary: [Nuxt.js docs](https://nuxtjs.org).

---

## 3 - Informations for developers

### 3.1 - Commit Standardization

- Standard language in English
- Text in lower case
- [Semantic commit](https://blog.cubos.io/que-tal-comecar-a-usar-commits-semanticos/)

- Semantic types:
  - feature
  - refactor
  - fix
  - chore
  - style
  - doc
  - test
- Example

```bash
    feature(component): create textfield
    component input data
```
## Vuex Patterns

No projeto as stores estão sendo encapsuladas na raiz do projeto.

```
.root
|
└───store
        actions.js
        getters.js
        index.js
        mutation-types.js
        mutations.js
        state.js
```
