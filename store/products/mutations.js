import * as types from './mutation-types'

export default {
  [types.GET_CATEGORY_MENU]: (state, categoryMenu) =>
    (state.categoryMenu = categoryMenu),
  [types.GET_PRODUCT]: (state, product) => (state.product = product),
}
