import * as types from './mutation-types'

export default {
  /**
   * Client - Get Menu
   */
  async getCategoryMenu({ commit }) {
    const data = await this.$axios.$get(
      `http://localhost:8888/api/V1/categories/list`
    )
    commit(types.GET_CATEGORY_MENU, data.items)
    return data.items
  },

  /**
   * Client - Get Product
   * @param params: {
   * }
   */
  async getProducts({ commit }, params) {
    const data = await this.$axios.$get(
      `http://localhost:8888/api/V1/categories/${params}`
    )
    commit(types.GET_PRODUCT, data.items)
    return data.items
  },
}
